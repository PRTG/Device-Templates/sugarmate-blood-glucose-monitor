# Sugarmate Blood Glucose Monitor 

PRTG REST Custom Sensor Template and Value Lookup file for use with the Sugarmate blood glucose tracker API

This repository contains the files needed to setup a REST Custom Sensor in PRTG, to retrieve and display blood glucose levels from the Sugarmate API. The Sugarmate software is designed to simplify diabetes management by retrieving, processing and displaying blood sugar levels obtained from a Continuous Glucose Monitor (CGM) device, such as the Dexcom G6.

**Installation & Use**

Extract the included ZIP file in the root of your PRTG installation (typically C:\Program Files (x86)\PRTG Network Monitor). This will extract the filea to their correct locations.

For detailed usage instructions, please see this blog article - https://blog.paessler.com/simplifying-diabetes-management-with-prtg

